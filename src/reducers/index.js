import {combineReducers} from 'redux';
import Loading from './Auth';
import auth from './Auth';
import {loadingBarReducer} from 'react-redux-loading-bar';

const export_list = {
  loadingBar: loadingBarReducer,
  auth
}

export default combineReducers(export_list);