import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import 'antd/dist/antd.css';
import {Home, Profile, Login} from './index';
import '../styles/main.css';
import LoadingBar from 'react-redux-loading-bar';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <LoadingBar/>
          <Route exact path='/' component={Home}/>
          <Route path='/u/:id' component={Profile}/>
          <Route path='/login' component={Login}/>
        </div>
      </Router>
    );
  }
}

export default App;
