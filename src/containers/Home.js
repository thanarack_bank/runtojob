import React, {Component} from 'react';
import {TopHeader, FeedList, FollowList} from '../components';
import {Row, Col} from 'antd';
import {connect} from 'react-redux';

class Home extends Component {
  constructor() {
    super();
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <TopHeader/>
        <Row>
          <div className='feed-warp margin-t-2'>
            <div className='c1'>
              <FeedList/>
            </div>
            <div className='c2'>
              <FollowList/>
            </div>
          </div>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {auth: state.auth};
}

export default connect(mapStateToProps)(Home);