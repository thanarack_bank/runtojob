import React, {Component} from 'react';
import {TopHeader, LoginForm} from '../components';
import {Row} from 'antd';
import {connect} from 'react-redux';

class Login extends Component {
  render() {
    return (
      <div>
        <TopHeader/>
        <Row>
          <div className='feed-warp margin-t-2'>
            <LoginForm/>
          </div>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return ({auth: state.auth});
}

export default connect(mapStateToProps)(Login);