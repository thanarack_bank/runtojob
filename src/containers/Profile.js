import React, {Component} from 'react';
import {TopHeader} from '../components';
import {Row, Button} from 'antd';
import {ProfileDashboard, ProfileDashboardSub, ProfileHead} from '../components';

class Profile extends Component {
  render() {
    return (
      <div>
        <TopHeader/>
        <Row>
          <div className='feed-warp profile margin-b-2'>
            <ProfileHead/>
            <div className='c1'>
              <ProfileDashboard/>
            </div>
            <div className='c2'>
              <Button
                type='primary'
                icon='download'
                className='export-btn-profile'
                size='large'>ดาวน์โหลดโปรไฟล์</Button>
              <ProfileDashboardSub/>
            </div>
          </div>
        </Row>
      </div>
    );
  }
}

export default Profile;