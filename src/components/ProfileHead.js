import React, {Component} from 'react';
import {Card, Avatar, Icon, Button} from 'antd';

class ProfileHead extends Component {
  render() {
    const data = {
      name: 'Thanarack chaisri',
      follower: 10,
      birt_day: '25 ปี 8 เดือน',
      avatar: 'https://scontent.fbkk1-5.fna.fbcdn.net/v/t1.0-9/18222340_868966859946605_7129420' +
          '470891345573_n.jpg?_nc_fx=fbkk1-4&_nc_eui2=v1%3AAeGYXnmXWTR2WS8-KDA-LsSa17Z31WY1' +
          'ycOXvDgB03BfEAMiBrfLUaF5pLDD4ysUDjilUNotTh0gLYSaF7MIwIiFqnXvsp7GCnrnFtiagrfeIw&o' +
          'h=6ec8ea6f022f1f8b209abb0fd861eb10&oe=5B411987',
      work: 'Full Stack at DigioThailand',
      address: 'Thailand, Yasothon'
    }
    return (
      <Card bordered={false} className='profile-head profile-head-text margin-b-2'>
        <div className='meta'>
          <img src={data.avatar}/>
          <ul className='data-user'>
            <li>{data.name}</li>
            <li><Icon type='home'/>{data.work}</li>
            <li><Icon type='environment-o'/>{data.address}</li>
            <li><Icon type='gift'/>{data.birt_day}</li>
            <li><Icon type='team'/>ผู้ติดตาม {data.follower}
              คน</li>
          </ul>
          <ul className='btn-user'>
            <li>
              <Button icon='form'>แก้ไขโปรไฟล์</Button>
            </li>
            <li>
              <Button>ติดตาม</Button>
            </li>
            <li>
              <Button>เกี่ยวกับ</Button>
            </li>
          </ul>
        </div>
      </Card>
    );
  }
}

export default ProfileHead;