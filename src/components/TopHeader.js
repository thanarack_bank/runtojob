import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {
  Layout,
  Menu,
  Breadcrumb,
  Icon,
  Row,
  Col,
  Dropdown,
  Avatar
} from 'antd';

const {SubMenu} = Menu;
const {Header, Content, Footer, Sider} = Layout;

class TopHeader extends Component {
  constructor() {
    super();
    this.state = {
      openDropdown: false
    }
  }
  handleClickAvatar = () => {
    this.setState(prevState => ({
      openDropdown: !prevState.openDropdown
    }))
  }
  render() {
    const {openDropdown} = this.state
    return (
      <Layout>
        <Header className='header'>
          <div className='top-warp'>
            <Link to='/'>
              <div className='logo'>
                <span>RUNTOJOB</span>
              </div>
            </Link>
            <ul className='menu-header right-header'>
              <li>
                <Link to='/'>หน้าแรกฟีด</Link>
              </li>
              <span tabIndex='0' className='profile-header'>
                <Avatar
                  src='https://pbs.twimg.com/profile_images/950028216189636610/V9H2rWlJ_bigger.jpg'/>
                <span className='down-icon'><Icon type='down'/></span>
                <ul className='dropdown-runtojob'>
                  <li>
                    <Link to='/u/123456789'>โปรไฟล์</Link>
                  </li>
                  <li>
                    <Link to='/setting'>ตั้งค่า</Link>
                  </li>
                  <li className='add_divine'>
                    <Link to='/favorite'>ที่บันทึกไว้</Link>
                  </li>
                  <li>
                    <Link to='/pages/create'>สร้างหน่วยงาน</Link>
                  </li>
                  <li>
                    <Link to='/help'>ความช่วยเหลือ</Link>
                  </li>
                  <li className='add_divine'>
                    <Link to='/'>ออกจากระบบ</Link>
                  </li>
                </ul>
              </span>
            </ul>
          </div>
        </Header>
      </Layout>
    );
  }
}

export default TopHeader;