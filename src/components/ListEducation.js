import React from 'react';
import {List, Avatar, Icon} from 'antd';
import {Post} from '../components';

const ListEducation = () => {
  const data = [
    {
      _id: '1',
      school: 'มหาวิทยาลัยราชภัฏอุบลราชธานี',
      education: 'วิทยาการคอมพิวเตอร์',
      degree: 'ปริญญาตรี',
      grade: '3.21',
      from_year: '2012',
      to_year: '2016'
    }, {
      _id: '2',
      school: 'ตระกูลประเทืองวิทยาคม',
      education: 'วิทย์คณิต',
      degree: 'มัธยม',
      grade: '2.46',
      from_year: '2012',
      to_year: '2016'
    }, {
      _id: '3',
      school: 'โรงเรียนบ้านราชมุนี',
      education: 'CEO',
      degree: 'ประถม',
      grade: '3.00',
      from_year: '2012',
      to_year: '2016'
    }
  ];

  const titleDom = item => (
    <div>
      <span className='company-work'>
        <span>{item.school}</span>
      </span>
      <span className='meta-education'>
        {(
          <span>
            <span><Icon type='book'/>{item.education}</span>
            <span>{item.degree}</span>
            <span>เกรด {item.grade}</span>
          </span>
        )}
      </span>
      <span className='meta-history-work'>
        <span>{item.from_year + ' - ' + item.to_year}</span>
      </span>
    </div>
  );

  const avatarDom = item => (
    <span>
      <Avatar
        className='avarta-company-work'
        size='large'
        shape='square'
        src='https://s3-ap-southeast-1.amazonaws.com/getlinks-files/static-web-image/img-default-education-2x.png'/>
    </span>
  );

  const descDom = item => (
    <span className='description-work'>
      {item.description}
    </span>
  );

  return (
    <div>
      <List
        className="list-history-work"
        itemLayout="horizontal"
        dataSource={data}
        renderItem={item => (
        <List.Item>
          <List.Item.Meta
            key={item._id}
            avatar={avatarDom(item)}
            title={titleDom(item)}
            description={descDom(item)}/>
        </List.Item>
      )}/>
    </div>
  );
}

export default ListEducation;