import React from 'react';
import {List, Avatar, Icon} from 'antd';
import {Post} from '../components';

const ListAchievement = () => {
  const data = [
    {
      _id: '1',
      title: 'ขายพระเครื่อง',
      project_name: 'สอบ.com',
      month: 'ธ.ค 2017',
      description: 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been' +
          ' the industrys standard dummy text ever since the 1500s, when an unknown printer' +
          ' took a galley of type and scrambled'
    }, {
      _id: '2',
      title: 'จัดการแข่งขันทัวนาเมนต์ทั่วประเทศ',
      project_name: 'amulet-thailand.com',
      month: 'ตุ.ค 2015',
      description: 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been' +
          ' the industrys standard dummy text ever since the 1500s, when an unknown printer' +
          ' took a galley of type and scrambled'
    }, {
      _id: '3',
      title: 'เว็บไชต์เกมส์เครือ Playpark',
      project_name: 'esports.playpark.com',
      month: 'เม.ย 2014',
      description: 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been' +
          ' the industrys standard dummy text ever since the 1500s, when an unknown printer' +
          ' took a galley of type and scrambled'
    }
  ];

  const titleDom = item => (
    <div>
      <span className='company-work'>
        <span>{item.title}</span>
      </span>
      <span className='project-name'>
        <span>{(
            <span>
              <span>{item.project_name}</span>
            </span>
          )}</span>
      </span>
    </div>
  );

  const monthDom = item => (
    <span className='ac-month'>
      <Icon type='calendar'/>
      <span>{item.month}</span>
    </span>
  );

  const descDom = item => (
    <span className='description-work'>
      {item.description}
    </span>
  );

  return (
    <div>
      <List
        className="list-history-achievement"
        itemLayout="horizontal"
        dataSource={data}
        renderItem={item => (
        <List.Item>
          <List.Item.Meta
            key={item._id}
            avatar={monthDom(item)}
            title={titleDom(item)}
            description={descDom(item)}/>
        </List.Item>
      )}/>
    </div>
  );
}

export default ListAchievement;