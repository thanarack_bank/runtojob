import React from 'react';
import {Icon, Avatar} from 'antd';
import {Link} from 'react-router-dom';

const Post = () => {
  return (
    <div className='post-box'>
      <div className='content'>
        <div className='post-head'>
          <Link to='/u/123' className='account-group'>
            <Avatar
              className='avatar'
              src='https://pbs.twimg.com/profile_images/924807176031412225/sTx7Khtq_400x400.jpg'/>
            <span className='fullname-group'>
              <span className='fullname'>กรมการทางหลวง</span>
            </span>
          </Link>
          <span className='icon-spec-line'></span>
          <span className='time'>
            <Link to='/post/123' className='run-timestamp'>
              โพสเมื่อ 3 มี.ค
            </Link>
          </span>
        </div>
        <div className='post-head-sub'>
          <span className='opening'><Icon type='check-circle'/>
            กำลังเปิดรับสมัคร</span>
          <span className='to'>ถึง 30 มี.ค 2560</span>
        </div>
        <div className='feed-text-container'>
          สภาวิศวกรรมแห่งประเทศไทย และกรมทางหลวง
          ลงพื้นที่ตรวจสอบรอยแยกของสะพานข้ามวงเวียนหลักสี่ฝั่ง ถ.แจ้งวัฒนะ
          มุ่งหน้ารามอินทรา ยืนยันว่า สะพานยังมีความมั่นคงแข็งแรง สามารถใช้งานได้ตามปกติ
        </div>
        <div className='post-footer'>
          <ul className='menu-footer'>
            <li>
              <a><Icon type='edit'/>
                <span>สมัครงาน</span>
              </a>
            </li>
            <li title='คนสนใจ'>
              <a><Icon type='smile-o'/>
                <span>658</span>
              </a>
            </li>
            <li title='ความเห็น'>
              <a><Icon type='message'/>
                <span>3,210</span>
              </a>
            </li>
            <li>
              <Link to='/post/123'><Icon type='profile'/>
                <span>รายละเอียด</span>
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Post;