import React from 'react';
import {List, Avatar, Icon} from 'antd';
import {Post} from '../components';

const ListHistoryWork = () => {
  const data = [
    {
      _id: '1',
      company_name: 'Digio',
      role: 'Web Developer',
      description: 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been' +
          ' the industrys standard dummy text ever since the 1500s, when an unknown printer' +
          ' took a galley of type and scrambled',
      location_country: 'ประเทศไทย',
      location_province: 'กรุงเทพ',
      month_from: 'ม.ค',
      year_from: '2557',
      month_to: 'ธ.ค',
      year_to: '2558'
    }, {
      _id: '2',
      company_name: 'Asset Bright',
      role: 'Full-Stack',
      description: 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been' +
          ' the industrys standard dummy text ever since the 1500s, when an unknown printer' +
          ' took a galley of type and scrambled',
      location_country: 'ประเทศไทย',
      location_province: 'กรุงเทพ',
      month_from: 'ม.ค',
      year_from: '2557',
      month_to: 'ธ.ค',
      year_to: '2558'
    }, {
      _id: '3',
      company_name: 'Ratmunee Studio 103.75',
      role: 'CEO',
      description: 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been' +
          ' the industrys standard dummy text ever since the 1500s, when an unknown printer' +
          ' took a galley of type and scrambled',
      location_country: 'ประเทศไทย',
      location_province: 'กรุงเทพ',
      month_from: 'ม.ค',
      year_from: '2557',
      month_to: 'ธ.ค',
      year_to: '2558'
    }
  ];

  const titleDom = item => (
    <div>
      <span className='company-work'>
        <span>{item.role}</span>
        <span>at</span>
        <span>{item.company_name}</span>
      </span>
      <span className='meta-history-work'>
        <span>{item.month_from + ' ' + item.year_from + ' - ' + item.month_to + ' ' + item.year_to + ' (5 months)'}

        </span>
        <span><Icon type='environment-o'/> {item.location_province + ', ' + item.location_country}</span>
      </span>
    </div>
  );

  const avatarDom = item => (
    <span>
      <Avatar
        className='avarta-company-work'
        size='large'
        shape='square'
        src='https://s3-ap-southeast-1.amazonaws.com/getlinks-files/static-web-image/img-default-company-2x.png'/>
    </span>
  );

  const descDom = item => (
    <span className='description-work'>
      {item.description}
    </span>
  );

  return (
    <div>
      <List
        className="list-history-work"
        itemLayout="horizontal"
        dataSource={data}
        renderItem={item => (
        <List.Item>
          <List.Item.Meta
            key={item._id}
            avatar={avatarDom(item)}
            title={titleDom(item)}
            description={descDom(item)}/>
        </List.Item>
      )}/>
    </div>
  );
}

export default ListHistoryWork;