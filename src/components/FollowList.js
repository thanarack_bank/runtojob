import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Card, List, Avatar, Button, Icon} from 'antd';

class FollowList extends Component {
  render() {
    const data = [
      {
        title: 'Ant Design Title 1'
      }, {
        title: 'Ant Design Title 2'
      }, {
        title: 'Ant Design Title 3'
      }, {
        title: 'Ant Design Title 4'
      }, {
        title: 'Ant Design Title 5'
      }
    ];
    return (
      <div>
        <Card bordered={false} className='follow-list-card' title='หน่วยงานที่น่าสนใจ'>
          <List
            itemLayout="horizontal"
            dataSource={data}
            renderItem={item => (
            <List.Item>
              <List.Item.Meta
                avatar={(<Avatar icon='user' size='large'/>)}
                title={(
                <Link to='/u/123'>{item.title}</Link>
              )}
                description={(
                <Button size='small'>
                  ติดตาม
                </Button>
              )}/>
            </List.Item>
          )}/>
          <div className='follow-list-footer'>
            <Link to='/'><Icon type='search'/>
              ค้นหาหน่วยงาน</Link>
          </div>
        </Card>
      </div>
    );
  }
}

export default FollowList;