import React, {Component} from 'react';
import {Rate} from 'antd';

class ListLanguage extends Component {
  render() {
    const data = [
      {
        language: 'ไทย',
        proficiency: 'เชี่ยวชาญ',
        level: 5
      }, {
        language: 'อังกฤษ',
        proficiency: 'เชี่ยวชาญ',
        level: 5
      }, {
        language: 'จีน',
        proficiency: 'ทั่วไป',
        level: 2.5
      }
    ]
    return (
      <div className='box-list-language'>
        <ul className='list-language'>
          {data.map((val, i) => (
            <li key={i}>
              <span>{val.language}</span>
              <span>{val.proficiency}</span>
              <span><Rate disabled defaultValue={val.level}/></span>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default ListLanguage;