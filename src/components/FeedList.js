import React from 'react';
import {Layout, Row, Col, Card} from 'antd';
import {Post} from '../components';

const FeedList = () => {
  return (
    <Layout>
      <Card bordered={false} className='feed-list-card'>
        <Post/>
      </Card>
    </Layout>
  );
}

export default FeedList;