import React, {Component} from 'react';
import {Card, Tag} from 'antd';
import {JobsActive, ListContact, ListLanguage} from '../components';

class ProfileDashboardSub extends Component {
  render() {
    return (
      <Card bordered={false} className='card-runtojob margin-t-1'>
        <section className='sec-profile'>
          <div className='headering'>
            <h3>งานที่คาดหวัง</h3>
          </div>
          <div className='body-section'>
            <JobsActive/>
          </div>
        </section>
        <section className='sec-profile'>
          <div className='headering'>
            <h3>การติดต่อ</h3>
          </div>
          <div className='body-section'>
            <ListContact/>
          </div>
        </section>
        <section className='sec-profile'>
          <div className='headering'>
            <h3>ความสามารถด้านภาษา</h3>
          </div>
          <div className='body-section'>
            <ListLanguage/>
          </div>
        </section>
      </Card>
    );
  }
}

export default ProfileDashboardSub;