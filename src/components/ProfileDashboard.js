import React, {Component} from 'react';
import {Card, Tag} from 'antd';
import {ListHistoryWork, ListSkillWork, IntroduceMySeft, ListEducation, ListAchievement} from '../components';

class ProfileDashboard extends Component {
  render() {
    return (
      <Card bordered={false} className='card-runtojob'>
        <section className='sec-profile'>
          <div className='headering'>
            <h3>เกี่ยวกับฉัน</h3>
          </div>
          <div className='body-section'>
            <IntroduceMySeft/>
          </div>
        </section>
        <section className='sec-profile margin-t-2'>
          <div className='headering'>
            <h3>ทักษะและความสามารถ</h3>
          </div>
          <div className='body-section'>
            <ListSkillWork/>
          </div>
        </section>
        <section className='sec-profile margin-t-2'>
          <div className='headering'>
            <h3>ประวัติด้านการทำงาน</h3>
          </div>
          <div className='body-section'>
            <ListHistoryWork/>
          </div>
        </section>
        <section className='sec-profile margin-t-2'>
          <div className='headering'>
            <h3>การศึกษา</h3>
          </div>
          <div className='body-section'>
            <ListEducation/>
          </div>
        </section>
        <section className='sec-profile margin-t-2'>
          <div className='headering'>
            <h3>ผลงานและรางวัลที่ได้รับ</h3>
          </div>
          <div className='body-section'>
            <ListAchievement/>
          </div>
        </section>
      </Card>
    );
  }
}

export default ProfileDashboard;