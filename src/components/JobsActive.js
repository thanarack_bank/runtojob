import React, {Component} from 'react';
import {Icon} from 'antd';

class JobsActive extends Component {
  render() {
    const data = {
      status_jobs: 'กำลังมองหางาน',
      type_work: 'เต็มเวลา',
      work_location: 'กรุงเทพ',
      salary: {
        current: 30000,
        expected: 50000,
        currency: 'บาท'
      }
    }
    return (
      <div className='job-active'>
        <ul className='list-job-active'>
          <li>
            <span className='find-job-enable'><Icon type='check-circle'/>{data.status_jobs}</span>
          </li>
          <li>
            <span>รูปแบบ : {data.type_work}</span>
          </li>
          <li>
            <span>ในจังหวัด : {data.work_location}</span>
          </li>
          <li></li>
          <li>
            <span><Icon type='pay-circle-o'/>
              เงินเดือน</span>
          </li>
          <li>
            <span>ปัจจุบัน : {data
                .salary
                .current
                .toLocaleString() + ' ' + data.salary.currency}</span>
          </li>
          <li>
            <span>ที่คาดหวัง : {data
                .salary
                .expected
                .toLocaleString() + ' ' + data.salary.currency}</span>
          </li>
        </ul>
      </div>
    );
  }
}

export default JobsActive;