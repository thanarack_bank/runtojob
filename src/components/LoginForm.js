import React from 'react';
import {
  Layout,
  Row,
  Col,
  Card,
  Icon,
  Form,
  Input,
  Button
} from 'antd';

const FormItem = Form.Item;

const LoginForm = () => {
  return (
    <Layout className='login-form'>
      <Card bordered={false}>
        <Row>
          <div className='login-header'>
            <h3>เข้าสู่ระบบ | RUNTOJOB</h3>
          </div>
          <Col span={12} offset={6}>
            <Form className="login-form">
              <FormItem>
                <Input
                  autoComplete='false'
                  size='large'
                  prefix={(<Icon
                  type='user'
                  style={{
                  color: 'rgba(0,0,0,.25)'
                }}/>)}
                  placeholder='Username'/>
              </FormItem>
              <FormItem>
                <Input
                  size='large'
                  prefix={(<Icon
                  type='lock'
                  style={{
                  color: 'rgba(0,0,0,.25)'
                }}/>)}
                  type='password'
                  placeholder='Password'/>
              </FormItem>
              <FormItem className='login-meta'>
                <Button
                  size='large'
                  type='primary'
                  htmlType='submit'
                  className='login-form-button'>
                  Log in
                </Button>
                <a className='login-form-forgot' href='/'>จำรหัสผ่านไม่ได้ ?</a>
                <a className='login-from-register' href='/'>สมัครใช้งานใหม่</a>
              </FormItem>
            </Form>
          </Col>
        </Row>
      </Card>
    </Layout>
  );
}

export default LoginForm;