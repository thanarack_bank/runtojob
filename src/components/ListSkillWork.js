import React from 'react';
import {Tag} from 'antd';

const ListSkillWork = () => {
  const data = {
    main_role: [
      'Full Stack Engineer', 'Frontend Engineer', 'Backend Engineer'
    ],
    total_exp_year: '5',
    mockSkillTag: [
      'php',
      'nodejs',
      'word2016',
      'excel2016',
      'maketing',
      'css',
      'html',
      'expreejs',
      'laravel',
      'react',
      'javascript',
      'mysql'
    ]
  }
  const renderSkill = data
    .mockSkillTag
    .map((val, i) => (
      <Tag key={i} color='#e1e6f0'>{val}</Tag>
    ));

  const renderRole = data
    .main_role
    .map((val, i) => ((
      <span key={i}>{val}</span>
    )))
  return (
    <div className='list-skill'>
      <p>
        <span>งานหลัก</span>
        <span className='list-role-main'>
          {renderRole}
        </span>
      </p>
      <p>
        <span>ประสบการณ์ทำงาน</span>
        <span className='total-exp-year'>{data.total_exp_year}</span>
        <span>ปี</span>
      </p>
      {renderSkill}
    </div>
  );
}

export default ListSkillWork;