import React, {Component} from 'react';
import {Icon} from 'antd';

class ListContact extends Component {
  render() {
    const data = {
      phone: '0827572447',
      mail: 'thanarackc@gmail.com',
      website: 'google.com',
      line: 'thanarackc',
      skype: 'bank_bralala'
    }
    return (
      <div className='box-list-contact'>
        <ul className='list-contact'>
          <li><Icon type='phone'/>{data.phone}</li>
          <li><Icon type='mail'/>{data.mail}</li>
          <li><Icon type='global'/>{data.website}</li>
          <li><Icon type='message'/>{data.line}</li>
          <li><Icon type='skype'/>{data.skype}</li>
        </ul>
      </div>
    );
  }
}

export default ListContact;